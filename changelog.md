# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2024-12-02
### Added
- It is now possible to deploy the K8s resource as a `DaemonSet` or a `Deployment`.

### Fixed
- Fix ephemeral-proof filter.
- USER_ATTRIBUTES header now contains also ephemeral proof attributes if they aren't assignable to roles

## [0.7.2] - 2024-12-14
### Changed
- now the configuration use `routes.abac[].identity-attributes` instead of `routes.abac[].roles`


## [0.7.1] - 2024-12-13
### Fixed
- Fix ephemeral-proof filter.


## [0.7.0] - 2024-12-02
### Added
- It is now possible to deploy the K8s resource as a `DaemonSet` or a `Deployment`.
- keypair algorithm as env variable `KEYPAIR_ALGORITHM`
- Gateway business logging

### Changed
- Changed header `Certificate-Id` with `Credential-Id`
- Adapt logic to use `Credential-Id` instead of `Participant-Id`

### Removed
- Removed password from keystore

### Fixed
- Fix ephemeral-proof filter
- Fix loading authority public key
