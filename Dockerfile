FROM eclipse-temurin:21-jdk-alpine
ENV CERTS_FOLDER=${CERTS_FOLDER:-/etc/certs/}
RUN adduser -S -u 1001 1001 && mkdir -p "$CERTS_FOLDER" && chmod 777 "$CERTS_FOLDER"
COPY target/*.jar app.jar
RUN chown 1001 /app.jar
USER 1001
ENTRYPOINT ["java","-jar","/app.jar"]
