package eu.europa.ec.simpl.tlsgateway.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.List;
import org.springframework.http.HttpStatus;

public class AbacException extends StatusException {

    public AbacException(List<String> rolePrivileges) {
        super(
                HttpStatus.UNAUTHORIZED,
                "You must have one of these identity attributes to perform this action, [ %s ]"
                        .formatted(String.join(", ", rolePrivileges)));
    }
}
