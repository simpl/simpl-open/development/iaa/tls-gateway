package eu.europa.ec.simpl.tlsgateway.configurations.publickey;

import eu.europa.ec.simpl.tlsgateway.configurations.microservices.UsersRolesProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("authority")
public class AuthorityPublicKeyProducer extends AbstractPublicKeyProducer {

    protected AuthorityPublicKeyProducer(UsersRolesProperties usersRolesProperties) {
        super(usersRolesProperties);
    }

    @Override
    protected String getAuthorityPublicKeyBaseUrl() {
        return getMyPublicKeyBaseUrl();
    }
}
