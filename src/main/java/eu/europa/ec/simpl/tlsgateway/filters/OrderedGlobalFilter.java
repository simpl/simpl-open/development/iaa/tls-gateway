package eu.europa.ec.simpl.tlsgateway.filters;

import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;

public abstract class OrderedGlobalFilter implements GlobalFilter, Ordered {

    private final GlobalFilterProperties globalFilterProperties;

    protected OrderedGlobalFilter(GlobalFilterProperties globalFilterProperties) {
        this.globalFilterProperties = globalFilterProperties;
    }

    @Override
    public int getOrder() {
        return globalFilterProperties.getOrder(getClass());
    }
}
