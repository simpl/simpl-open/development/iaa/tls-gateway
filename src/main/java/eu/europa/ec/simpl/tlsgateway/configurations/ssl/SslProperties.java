package eu.europa.ec.simpl.tlsgateway.configurations.ssl;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.server.Ssl;

@ConfigurationProperties("custom-ssl")
public record SslProperties(
        Boolean enabled,
        String keyStoreLocation,
        String keyStorePassword,
        Ssl.ClientAuth clientAuth,
        String trustStoreLocation,
        String trustStorePassword,
        String keyStoreType,
        String trustStoreType) {}
