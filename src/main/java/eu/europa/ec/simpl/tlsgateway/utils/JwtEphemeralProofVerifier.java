package eu.europa.ec.simpl.tlsgateway.utils;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.ephemeralproof.EphemeralProofAbstractParser;
import eu.europa.ec.simpl.common.ephemeralproof.JwtEphemeralProofParser;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import java.security.interfaces.ECPublicKey;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class JwtEphemeralProofVerifier extends EphemeralProofAbstractParser<SignedJWT> {

    private final JwtEphemeralProofParser jwtEphemeralProofParser;

    public JwtEphemeralProofVerifier(JwtEphemeralProofParser jwtEphemeralProofParser) {
        super(jwtEphemeralProofParser.getEphemeralProof());
        this.jwtEphemeralProofParser = jwtEphemeralProofParser;
    }

    public JwtEphemeralProofVerifier(String ephemeralProof) {
        this(new JwtEphemeralProofParser(ephemeralProof));
    }

    @Override
    public String getRaw() {
        return jwtEphemeralProofParser.getRaw();
    }

    @Override
    public UUID getSubject() {
        return jwtEphemeralProofParser.getSubject();
    }

    @Override
    public List<IdentityAttributeDTO> getIdentityAttributes() {
        return jwtEphemeralProofParser.getIdentityAttributes();
    }

    @Override
    public List<String> getPublicKeys() {
        return jwtEphemeralProofParser.getPublicKeys();
    }

    @Override
    public Instant getExpiration() {
        return jwtEphemeralProofParser.getExpiration();
    }

    public JWTClaimsSet getClaimsSet() {
        return jwtEphemeralProofParser.getClaimsSet();
    }

    public boolean verify(ECPublicKey publicKey) throws JOSEException {
        return getEphemeralProof().verify(new ECDSAVerifier(publicKey));
    }
}
