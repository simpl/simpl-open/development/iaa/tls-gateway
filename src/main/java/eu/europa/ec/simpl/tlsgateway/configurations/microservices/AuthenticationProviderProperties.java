package eu.europa.ec.simpl.tlsgateway.configurations.microservices;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("authentication-provider")
public record AuthenticationProviderProperties(String url) {}
