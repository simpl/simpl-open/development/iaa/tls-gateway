package eu.europa.ec.simpl.tlsgateway.configurations.security;

import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
import org.springframework.util.CollectionUtils;

@Log4j2
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    private final RouteConfig routeConfig;
    private final List<ServerWebExchangeMatcher> publicMatchers;
    private final List<ServerWebExchangeMatcher> deniedMatchers;

    public SecurityConfig(RouteConfig routeConfig) {
        this.routeConfig = routeConfig;
        this.publicMatchers = buildMatchers(routeConfig.publicUrls());
        this.deniedMatchers = buildMatchers(routeConfig.deniedUrls());
    }

    @Bean
    @Primary
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {

        return http.authorizeExchange(exchange -> {
                    if (!CollectionUtils.isEmpty(routeConfig.publicUrls())) {
                        log.info("configuring public urls {}", routeConfig.publicUrls());
                        exchange.matchers(publicMatchers.toArray(ServerWebExchangeMatcher[]::new))
                                .permitAll();
                    }

                    if (!CollectionUtils.isEmpty(routeConfig.deniedUrls())) {
                        log.info("configuring urls to deny {}", routeConfig.publicUrls());
                        exchange.matchers(deniedMatchers.toArray(ServerWebExchangeMatcher[]::new))
                                .denyAll();
                    }

                    exchange.anyExchange().permitAll();
                })
                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .build();
    }

    private List<ServerWebExchangeMatcher> buildMatchers(List<Rule> rules) {
        if (rules == null) {
            return List.of();
        }
        return rules.stream()
                .map(rule -> ServerWebExchangeMatchers.pathMatchers(rule.method(), rule.path()))
                .toList();
    }
}
