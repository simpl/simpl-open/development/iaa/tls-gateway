package eu.europa.ec.simpl.tlsgateway.configurations.security;

import eu.europa.ec.simpl.tlsgateway.configurations.LoggingRule;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "routes")
public record RouteConfig(List<Rule> publicUrls, List<Rule> deniedUrls, List<Rule> abac, Logging logging) {

    public record Logging(List<LoggingRule> business) {}
}
