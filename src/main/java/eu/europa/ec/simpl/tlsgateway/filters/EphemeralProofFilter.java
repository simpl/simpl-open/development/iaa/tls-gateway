package eu.europa.ec.simpl.tlsgateway.filters;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.api.authenticationprovider.v1.model.CredentialDTO;
import eu.europa.ec.simpl.common.ephemeralproof.EphemeralProofAbstractParser;
import eu.europa.ec.simpl.common.exceptions.EphemeralProofNotFoundException;
import eu.europa.ec.simpl.common.exceptions.InvalidEphemeralProofException;
import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import eu.europa.ec.simpl.common.utils.Sha384Converter;
import eu.europa.ec.simpl.tlsgateway.configurations.AuthorityKeypairProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.security.RouteConfig;
import eu.europa.ec.simpl.tlsgateway.configurations.security.Rule;
import eu.europa.ec.simpl.tlsgateway.repositories.EphemeralProofRepository;
import eu.europa.ec.simpl.tlsgateway.utils.ExchangeUtil;
import eu.europa.ec.simpl.tlsgateway.utils.JwtEphemeralProofVerifier;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Component
public class EphemeralProofFilter extends OrderedGlobalFilter {

    protected static final String MANAGE_EPHEMERAL_PROOF = "MANAGE_EPHEMERAL_PROOF";
    protected static final String VALID_EPHEMERAL_PROOF = "VALID_EPHEMERAL_PROOF";
    protected static final String EPHEMERAL_PROOF_ATTRIBUTES = "EPHEMERAL_PROOF_ATTRIBUTES";

    private final RouteConfig routeConfig;
    private final Function<String, JwtEphemeralProofVerifier> ephemeralProofValidatorFactory;
    private final EphemeralProofRepository ephemeralProofRepository;
    private final CredentialDTO authorityKey;
    private final AuthorityKeypairProperties authorityKeypairProperties;

    public EphemeralProofFilter(
            RouteConfig routeConfig,
            EphemeralProofRepository ephemeralProofRepository,
            @Autowired(required = false) Function<String, JwtEphemeralProofVerifier> ephemeralProofVerifierFactory,
            @Qualifier("authorityPublicKey") CredentialDTO authorityKey,
            GlobalFilterProperties globalFilterProperties,
            AuthorityKeypairProperties authorityKeypairProperties) {
        super(globalFilterProperties);
        this.routeConfig = routeConfig;
        this.ephemeralProofRepository = ephemeralProofRepository;
        this.ephemeralProofValidatorFactory =
                Objects.requireNonNullElse(ephemeralProofVerifierFactory, JwtEphemeralProofVerifier::new);
        this.authorityKey = authorityKey;
        this.authorityKeypairProperties = authorityKeypairProperties;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("Ephemeral Proof Filter...");
        return Flux.fromIterable(routeConfig.abac())
                .filterWhen(rule -> rule.isMatch(exchange))
                .next()
                .switchIfEmpty(manageNonAbacRoute(exchange))
                .flatMap(rule -> manageAbacRoute(rule, exchange))
                .then(chain.filter(exchange));
    }

    private Mono<Rule> manageNonAbacRoute(ServerWebExchange exchange) {
        return Mono.defer(() -> {
            log.debug("manageNonAbacRoute");
            exchange.getAttributes().put(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
            checkEphemeralProof(exchange);
            return Mono.empty();
        });
    }

    private Mono<Rule> manageAbacRoute(Rule rule, ServerWebExchange exchange) {
        log.debug("manageAbacRoute {}", rule);
        if (!rule.disableEphemeralProofCheck()) {
            exchange.getAttributes().put(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
            checkEphemeralProof(exchange);
        } else {
            exchange.getAttributes().put(MANAGE_EPHEMERAL_PROOF, Boolean.FALSE);
        }
        return Mono.empty();
    }

    private void checkEphemeralProof(ServerWebExchange exchange) {
        var credentialId = ExchangeUtil.getCredentialIdFromExchange(exchange);
        var ephemeralProof = ephemeralProofRepository
                .findById(credentialId)
                .map(EphemeralProof::getContent)
                .filter(Predicate.not(String::isBlank))
                .orElse(null);
        exchange.getAttributes().put(VALID_EPHEMERAL_PROOF, isValidProof(ephemeralProof, exchange));
    }

    private boolean isValidProof(String ephemeralProof, ServerWebExchange exchange) {
        var isValidProof = StringUtils.hasText(ephemeralProof);
        var manageProof = (boolean) exchange.getAttributes().get(MANAGE_EPHEMERAL_PROOF);
        if (isValidProof) {
            var verifier = ephemeralProofValidatorFactory.apply(ephemeralProof);
            checkIfValidSign(verifier);
            checkIfSameTierTwoPublicKey(verifier, ExchangeUtil.getCertificateFromRequest(exchange));
            addEphemeralProofAttributesToSession(verifier, exchange);
        } else if (manageProof) {
            throw new EphemeralProofNotFoundException();
        } else {
            log.warn("No ephemeral proof found. Starting OCSP check...");
        }
        return isValidProof;
    }

    private void addEphemeralProofAttributesToSession(
            EphemeralProofAbstractParser<SignedJWT> ephemeralProof, ServerWebExchange exchange) {
        exchange.getAttributes().put(EPHEMERAL_PROOF_ATTRIBUTES, ephemeralProof.getIdentityAttributes());
    }

    private void checkIfSameTierTwoPublicKey(
            EphemeralProofAbstractParser<SignedJWT> ephemeralProof, X509Certificate cert) {
        var isValid = Objects.equals(
                Sha384Converter.toSha384(cert.getPublicKey()),
                ephemeralProof.getPublicKeys().getFirst());
        if (!isValid) {
            throw new InvalidEphemeralProofException();
        }
        log.debug("certificate public key is the same found in ephemeral proof");
    }

    private void checkIfValidSign(JwtEphemeralProofVerifier verifier) {
        try {
            if (!verifier.verify(loadPublicKey())) {
                log.error("JWT did not match the given signature");
                throw new InvalidEphemeralProofException();
            }
        } catch (JOSEException e) {
            log.error("Invalid JWT", e);
            throw new InvalidEphemeralProofException();
        }
    }

    protected ECPublicKey loadPublicKey() {
        log.debug("Loading public key from authority...");

        var publicKey = Base64.getDecoder().decode(authorityKey.getPublicKey());
        try {
            return (ECPublicKey) KeyFactory.getInstance(authorityKeypairProperties.algorithm())
                    .generatePublic(new X509EncodedKeySpec(publicKey));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            throw new RuntimeWrapperException(e);
        }
    }
}
