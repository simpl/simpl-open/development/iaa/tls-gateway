package eu.europa.ec.simpl.tlsgateway.filters;

import eu.europa.ec.simpl.client.util.CertificateRevocation;
import eu.europa.ec.simpl.client.util.DaggerCertificateRevocationFactory;
import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import eu.europa.ec.simpl.tlsgateway.exceptions.InvalidCertificateException;
import eu.europa.ec.simpl.tlsgateway.utils.ExchangeUtil;
import java.security.cert.CertificateException;
import java.util.Objects;
import java.util.function.Supplier;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Log4j2
@Component
public class OcspFilter extends OrderedGlobalFilter {

    private final Supplier<CertificateRevocation> certificateRevocationFactory;

    public OcspFilter(
            @Autowired(required = false) Supplier<CertificateRevocation> certificateRevocationFactory,
            GlobalFilterProperties globalFilterProperties) {
        super(globalFilterProperties);
        this.certificateRevocationFactory = Objects.requireNonNullElse(
                certificateRevocationFactory,
                () -> DaggerCertificateRevocationFactory.create().get());
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if (shouldSkip(exchange)) {
            return chain.filter(exchange);
        }
        log.info("Ocsp Filter...");
        var cert = ExchangeUtil.getCertificateFromRequest(exchange);
        try {
            certificateRevocationFactory.get().verify(cert);
        } catch (CertificateException e) {
            throw new InvalidCertificateException(e);
        }
        return chain.filter(exchange);
    }

    private boolean shouldSkip(ServerWebExchange exchange) {
        return Objects.equals(Boolean.TRUE, exchange.getAttribute(EphemeralProofFilter.VALID_EPHEMERAL_PROOF));
    }
}
