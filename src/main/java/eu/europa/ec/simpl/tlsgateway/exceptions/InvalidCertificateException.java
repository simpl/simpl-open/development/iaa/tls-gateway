package eu.europa.ec.simpl.tlsgateway.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class InvalidCertificateException extends RuntimeException {
    public InvalidCertificateException(String message) {
        super(message);
    }

    public InvalidCertificateException(Throwable cause) {
        super(cause);
    }
}
