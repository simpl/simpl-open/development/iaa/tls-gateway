package eu.europa.ec.simpl.tlsgateway.exceptions;

public class InvalidPrivateKeyException extends RuntimeException {
    public InvalidPrivateKeyException(String message) {
        super(message);
    }

    public InvalidPrivateKeyException(Throwable cause) {
        super(cause);
    }
}
