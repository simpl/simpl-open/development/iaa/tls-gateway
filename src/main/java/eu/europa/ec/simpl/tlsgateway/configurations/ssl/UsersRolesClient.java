package eu.europa.ec.simpl.tlsgateway.configurations.ssl;

import eu.europa.ec.simpl.tlsgateway.configurations.microservices.UsersRolesProperties;
import eu.europa.ec.simpl.tlsgateway.exceptions.InvalidCertificateException;
import java.nio.charset.StandardCharsets;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class UsersRolesClient {
    private final UsersRolesProperties usersRolesProperties;

    public UsersRolesClient(UsersRolesProperties usersRolesProperties) {
        this.usersRolesProperties = usersRolesProperties;
    }

    public String loadPemCertificates() {
        return WebClient.builder()
                .baseUrl("%s/credential/download".formatted(usersRolesProperties.url()))
                .defaultHeader("Content-Type", MediaType.APPLICATION_OCTET_STREAM_VALUE)
                .build()
                .get()
                .retrieve()
                .bodyToMono(byte[].class)
                .map(res -> new String(res, StandardCharsets.UTF_8))
                .doOnError(e -> {
                    throw new InvalidCertificateException(
                            "Failed to load certificate from %s".formatted(usersRolesProperties.url()));
                })
                .block();
    }
}
