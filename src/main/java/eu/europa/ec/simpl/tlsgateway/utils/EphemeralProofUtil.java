package eu.europa.ec.simpl.tlsgateway.utils;

import eu.europa.ec.simpl.common.ephemeralproof.JwtEphemeralProofParser;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

@Log4j2
@UtilityClass
public class EphemeralProofUtil {

    public static JwtEphemeralProofParser getEphemeralProof(String ephemeralProof) {
        return new JwtEphemeralProofParser(ephemeralProof);
    }
}
