package eu.europa.ec.simpl.tlsgateway.services;

import eu.europa.ec.simpl.common.ephemeralproof.JwtEphemeralProofParser;
import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import eu.europa.ec.simpl.common.services.AbstractTierOneSessionValidator;
import eu.europa.ec.simpl.tlsgateway.repositories.EphemeralProofRepository;
import java.util.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class TierOneSessionValidator extends AbstractTierOneSessionValidator<JwtEphemeralProofParser> {

    private final EphemeralProofRepository repository;

    public TierOneSessionValidator(EphemeralProofRepository repository) {
        this.repository = repository;
    }

    @Override
    protected Optional<EphemeralProof> fetchEphemeralProofById(String credentialId) {
        return repository.findById(credentialId);
    }

    @Override
    protected JwtEphemeralProofParser getEphemeralProofParser(String rawEphemeralProof) {
        return new JwtEphemeralProofParser(rawEphemeralProof);
    }
}
