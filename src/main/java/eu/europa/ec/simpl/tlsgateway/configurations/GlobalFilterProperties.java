package eu.europa.ec.simpl.tlsgateway.configurations;

import eu.europa.ec.simpl.tlsgateway.filters.OrderedGlobalFilter;
import java.util.List;
import java.util.Optional;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.Ordered;

@ConfigurationProperties(prefix = "global-filters")
public class GlobalFilterProperties {

    private final List<Class<? extends OrderedGlobalFilter>> order;

    public GlobalFilterProperties(List<Class<? extends OrderedGlobalFilter>> order) {
        this.order = order;
    }

    public int getOrder(Class<? extends OrderedGlobalFilter> filterClass) {
        return Optional.ofNullable(filterClass)
                .map(order::indexOf)
                .map(i -> Ordered.HIGHEST_PRECEDENCE + i)
                .orElse(Ordered.LOWEST_PRECEDENCE);
    }
}
