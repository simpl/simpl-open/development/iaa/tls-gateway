package eu.europa.ec.simpl.tlsgateway.filters;

import static eu.simpl.MessageBuilder.buildMessage;

import eu.europa.ec.simpl.api.authenticationprovider.v1.model.CredentialDTO;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import eu.europa.ec.simpl.common.utils.Sha384Converter;
import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.LoggingRule;
import eu.europa.ec.simpl.tlsgateway.configurations.security.RouteConfig;
import eu.europa.ec.simpl.tlsgateway.utils.ExchangeUtil;
import eu.simpl.types.LogMessage;
import eu.simpl.types.MessageType;
import java.util.*;
import java.util.function.Function;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Component
public class LoggingFilter extends OrderedGlobalFilter {

    private final CredentialDTO myPublicKey;
    private final List<LoggingRule> loggingRules;

    public LoggingFilter(
            @Qualifier("myPublicKey") CredentialDTO myPublicKey,
            RouteConfig routeConfig,
            GlobalFilterProperties globalFilterProperties) {
        super(globalFilterProperties);
        this.myPublicKey = myPublicKey;
        this.loggingRules = Optional.ofNullable(routeConfig)
                .map(RouteConfig::logging)
                .map(RouteConfig.Logging::business)
                .orElse(List.of());
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return Flux.fromIterable(loggingRules)
                .filterWhen(rule -> rule.matches(exchange))
                .next()
                .map(buildLogger(exchange, chain))
                .flatMap(BusinessLogger::log)
                .switchIfEmpty(chain.filter(exchange));
    }

    private Function<LoggingRule, BusinessLogger> buildLogger(ServerWebExchange exchange, GatewayFilterChain chain) {
        return loggingRule -> new BusinessLogger(loggingRule.config(), exchange, chain);
    }

    protected BusinessLogger newBusinessLogger(
            LoggingRule.Config config, ServerWebExchange exchange, GatewayFilterChain chain) {
        return new BusinessLogger(config, exchange, chain);
    }

    protected class BusinessLogger {

        private final LoggingRule.Config config;
        private final ServerWebExchange exchange;
        private final GatewayFilterChain chain;
        private final UUID correlationId;

        private BusinessLogger(LoggingRule.Config config, ServerWebExchange exchange, GatewayFilterChain chain) {
            this.config = config;
            this.exchange = exchange;
            this.chain = chain;
            this.correlationId = UUID.randomUUID();
        }

        public Mono<Void> log() {
            return log(MessageType.REQUEST)
                    .then(chain.filter(exchange).doAfterTerminate(() -> log(MessageType.RESPONSE)));
        }

        private Mono<Void> log(MessageType messageType) {
            var token = JwtUtil.getBearerToken(exchange.getRequest().getHeaders());

            var user = token.map(getUser()).orElse("anonymous user");

            Map<String, String> templateContext = getTemplateContext(messageType, user);

            var messageTemplate = config.getMessage();

            log.log(
                    Level.getLevel("BUSINESS"),
                    buildMessage(LogMessage.builder()
                            .businessOperations(config.getOperations())
                            .messageType(messageType)
                            .origin(getOrigin(messageType))
                            .destination(getDestination(messageType))
                            .correlationId(correlationId.toString())
                            .msg(StrSubstitutor.replace(messageTemplate, templateContext))
                            .build()));

            return Mono.empty();
        }

        private String getOrigin(MessageType messageType) {
            return switch (messageType) {
                case REQUEST -> ExchangeUtil.getCredentialIdFromExchange(exchange);
                case RESPONSE -> getMyCredentialId();
                default -> throw new IllegalArgumentException("Unsupported message type %s".formatted(messageType));
            };
        }

        private String getDestination(MessageType messageType) {
            return switch (messageType) {
                case REQUEST -> getMyCredentialId();
                case RESPONSE -> ExchangeUtil.getCredentialIdFromExchange(exchange);
                default -> throw new IllegalArgumentException("Unsupported message type %s".formatted(messageType));
            };
        }

        private String getMyCredentialId() {
            return Sha384Converter.toSha384(Base64.getDecoder().decode(myPublicKey.getPublicKey()));
        }

        private Function<String, String> getUser() {
            return token -> JwtUtil.getClaim(JwtUtil.parseJwt(token), "email").toString();
        }

        private Map<String, String> getTemplateContext(MessageType messageType, String user) {
            return Map.of(
                    "method", exchange.getRequest().getMethod().toString(),
                    "path", exchange.getRequest().getPath().value(),
                    "user", user,
                    "responseStatus", getResponseStatus(messageType));
        }

        private String getResponseStatus(MessageType messageType) {
            if (messageType == MessageType.REQUEST) {
                return "";
            } else {
                return Optional.of(exchange.getResponse())
                        .map(ServerHttpResponse::getStatusCode)
                        .map(Objects::toString)
                        .orElse("");
            }
        }
    }
}
