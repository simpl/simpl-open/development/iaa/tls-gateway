package eu.europa.ec.simpl.tlsgateway.filters;

import static eu.europa.ec.simpl.tlsgateway.filters.EphemeralProofFilter.EPHEMERAL_PROOF_ATTRIBUTES;
import static eu.europa.ec.simpl.tlsgateway.filters.EphemeralProofFilter.MANAGE_EPHEMERAL_PROOF;

import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.constants.SimplHeaders;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import eu.europa.ec.simpl.tlsgateway.services.TierOneSessionValidator;
import eu.europa.ec.simpl.tlsgateway.utils.ExchangeUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Log4j2
@Component
public class HeadersFilter extends OrderedGlobalFilter {

    private final TierOneSessionValidator tierOneSessionValidator;

    public HeadersFilter(
            TierOneSessionValidator tierOneSessionValidator, GlobalFilterProperties globalFilterProperties) {
        super(globalFilterProperties);
        this.tierOneSessionValidator = tierOneSessionValidator;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange serverWebExchange, GatewayFilterChain chain) {
        log.info("Headers Filter...");
        var credentialId = ExchangeUtil.getCredentialIdFromExchange(serverWebExchange);
        return Mono.just(serverWebExchange).flatMap(exchange -> {
            var jwt = getValidJwtOrThrow(serverWebExchange, exchange, credentialId);
            var attributes = getIdentityAttributes(exchange, jwt);
            exchange = decorateRequest(exchange, attributes, credentialId);
            return chain.filter(exchange);
        });
    }

    private ServerWebExchange decorateRequest(
            ServerWebExchange exchange, List<String> attributes, String credentialId) {
        exchange = addAbacAttributeToRequest(exchange, attributes);
        exchange = addHeadersToRequest(exchange, credentialId);
        return exchange;
    }

    private List<String> getIdentityAttributes(ServerWebExchange exchange, Optional<SignedJWT> jwt) {
        var epAttributes = getIdentityAttributeCodes(getEphemeralProofAttributes(exchange));
        var endUserAttributes = jwt.map(this::getIdentityAttributeCodes).orElse(List.of()).stream();
        return Stream.concat(epAttributes, endUserAttributes).toList();
    }

    private Optional<SignedJWT> getValidJwtOrThrow(
            ServerWebExchange serverWebExchange, ServerWebExchange exchange, String credentialId) {
        var jwt = getTierOneToken(exchange);
        jwt.ifPresent(signedJWT -> validateSession(serverWebExchange, signedJWT, credentialId));
        return jwt;
    }

    private ServerWebExchange addAbacAttributeToRequest(ServerWebExchange exchange, List<String> attributes) {
        if (!attributes.isEmpty()) {
            return exchange.mutate()
                    .request(request -> request.header(
                                    SimplHeaders.USER_ATTRIBUTES, buildUserAttributesHeader(attributes.stream()))
                            .build())
                    .build();
        }
        return exchange;
    }

    @SuppressWarnings("unchecked")
    private List<IdentityAttributeDTO> getEphemeralProofAttributes(ServerWebExchange exchange) {
        if (Boolean.TRUE.equals(exchange.getAttributes().get(MANAGE_EPHEMERAL_PROOF))) {
            return (List<IdentityAttributeDTO>) exchange.getAttributes().get(EPHEMERAL_PROOF_ATTRIBUTES);
        }
        return new ArrayList<>();
    }

    private ServerWebExchange addHeadersToRequest(ServerWebExchange exchange, String credentialId) {
        return exchange.mutate()
                .request(request -> request.header(SimplHeaders.CREDENTIAL_ID, credentialId))
                .build();
    }

    private Optional<SignedJWT> getTierOneToken(ServerWebExchange exchange) {
        var shouldManage = exchange.getAttributes().get(MANAGE_EPHEMERAL_PROOF);
        if (Boolean.TRUE.equals(shouldManage)) {
            return parseToken(exchange);
        }
        return Optional.empty();
    }

    private Optional<SignedJWT> parseToken(ServerWebExchange exchange) {
        return JwtUtil.getBearerToken(exchange.getRequest().getHeaders()).map(JwtUtil::parseJwt);
    }

    private void validateSession(ServerWebExchange exchange, SignedJWT tierOneJwt, String credentialId) {
        if (Objects.equals(Boolean.TRUE, exchange.getAttributes().get(MANAGE_EPHEMERAL_PROOF))) {
            tierOneSessionValidator.validate(tierOneJwt, credentialId);
        }
    }

    @SneakyThrows
    private List<String> getIdentityAttributeCodes(SignedJWT jwt) {
        return Optional.ofNullable(jwt)
                .flatMap(token -> Optional.ofNullable(JwtUtil.getListClaim(token, "identity_attributes", String.class)))
                .orElse(new ArrayList<>());
    }

    private String buildUserAttributesHeader(Stream<String> attributes) {
        return attributes.collect(Collectors.joining(","));
    }

    private Stream<String> getIdentityAttributeCodes(List<IdentityAttributeDTO> list) {
        return list.stream()
                .filter(Predicate.not(IdentityAttributeDTO::isAssignableToRoles))
                .map(IdentityAttributeDTO::getCode);
    }
}
