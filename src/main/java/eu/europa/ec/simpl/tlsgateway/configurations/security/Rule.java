package eu.europa.ec.simpl.tlsgateway.configurations.security;

import eu.europa.ec.simpl.common.constants.SimplHeaders;
import eu.europa.ec.simpl.tlsgateway.exceptions.AbacException;
import java.util.*;
import java.util.function.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
public record Rule(
        HttpMethod method, String path, boolean disableEphemeralProofCheck, List<String> identityAttributes) {

    public Mono<Boolean> isMatch(ServerWebExchange exchange) {
        return ServerWebExchangeMatchers.pathMatchers(method, path)
                .matches(exchange)
                .map(ServerWebExchangeMatcher.MatchResult::isMatch);
    }

    public boolean hasAttributes() {
        return !CollectionUtils.isEmpty(identityAttributes);
    }

    public Mono<Void> checkPrivilege(ServerWebExchange exchange) {
        var userIdentityAttributes =
                exchange.getRequest().getHeaders().getOrEmpty(SimplHeaders.USER_ATTRIBUTES).stream()
                        .filter(Objects::nonNull)
                        .filter(Predicate.not(String::isBlank))
                        .findFirst()
                        .map(attributes -> Arrays.asList(attributes.split(",")))
                        .orElseThrow(() -> new AbacException(identityAttributes));

        if (Collections.disjoint(userIdentityAttributes, identityAttributes)) {
            throw new AbacException(identityAttributes);
        }

        return Mono.empty();
    }
}
