package eu.europa.ec.simpl.tlsgateway.configurations.microservices;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "users-roles")
public record UsersRolesProperties(String url) {}
