package eu.europa.ec.simpl.tlsgateway.repositories;

import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnBean(RedisConnectionFactory.class)
public interface EphemeralProofRepository extends CrudRepository<EphemeralProof, String> {}
