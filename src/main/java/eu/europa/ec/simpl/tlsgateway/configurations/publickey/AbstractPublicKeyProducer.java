package eu.europa.ec.simpl.tlsgateway.configurations.publickey;

import eu.europa.ec.simpl.api.authenticationprovider.v1.model.CredentialDTO;
import eu.europa.ec.simpl.tlsgateway.configurations.microservices.UsersRolesProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

public abstract class AbstractPublicKeyProducer {

    protected final UsersRolesProperties usersRolesProperties;

    protected AbstractPublicKeyProducer(UsersRolesProperties usersRolesProperties) {
        this.usersRolesProperties = usersRolesProperties;
    }

    protected abstract String getAuthorityPublicKeyBaseUrl();

    protected String getMyPublicKeyBaseUrl() {
        return usersRolesProperties.url();
    }

    @Bean
    public CredentialDTO authorityPublicKey() {
        return getPublicKey(getAuthorityPublicKeyBaseUrl());
    }

    @Bean
    public CredentialDTO myPublicKey() {
        return getPublicKey(getMyPublicKeyBaseUrl());
    }

    private CredentialDTO getPublicKey(String url) {
        return WebClient.builder()
                .baseUrl("%s/v1/credentials/publicKey".formatted(url))
                .defaultHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .build()
                .get()
                .retrieve()
                .bodyToMono(CredentialDTO.class)
                .block();
    }
}
