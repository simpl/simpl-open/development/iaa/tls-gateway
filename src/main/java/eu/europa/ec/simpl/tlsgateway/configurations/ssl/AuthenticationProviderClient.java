package eu.europa.ec.simpl.tlsgateway.configurations.ssl;

import eu.europa.ec.simpl.api.authenticationprovider.v1.model.KeyPairDTO;
import eu.europa.ec.simpl.common.utils.CredentialUtil;
import eu.europa.ec.simpl.tlsgateway.configurations.AuthorityKeypairProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.microservices.AuthenticationProviderProperties;
import eu.europa.ec.simpl.tlsgateway.exceptions.InvalidCertificateException;
import eu.europa.ec.simpl.tlsgateway.exceptions.InvalidPrivateKeyException;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import lombok.SneakyThrows;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class AuthenticationProviderClient {

    private final AuthorityKeypairProperties authorityKeypairProperties;
    private final AuthenticationProviderProperties authenticationProviderProperties;

    public AuthenticationProviderClient(
            AuthenticationProviderProperties authenticationProviderProperties,
            AuthorityKeypairProperties authorityKeypairProperties) {
        this.authenticationProviderProperties = authenticationProviderProperties;
        this.authorityKeypairProperties = authorityKeypairProperties;
    }

    @SneakyThrows
    public PrivateKey loadPrivateKey() {
        return WebClient.builder()
                .baseUrl("%s/v1/keypairs".formatted(authenticationProviderProperties.url()))
                .build()
                .get()
                .retrieve()
                .bodyToMono(KeyPairDTO.class)
                .map(KeyPairDTO::getPrivateKey)
                .map(privateKey -> CredentialUtil.loadPrivateKey(privateKey, authorityKeypairProperties.algorithm()))
                .doOnError(e -> {
                    throw new InvalidPrivateKeyException(
                            "Failed to load private key from %s".formatted(authenticationProviderProperties.url()));
                })
                .block();
    }

    public String loadPemCertificates() {
        return WebClient.builder()
                .baseUrl("%s/v1/credentials/download".formatted(authenticationProviderProperties.url()))
                .defaultHeader("Content-Type", MediaType.APPLICATION_OCTET_STREAM_VALUE)
                .build()
                .get()
                .retrieve()
                .bodyToMono(byte[].class)
                .map(res -> new String(res, StandardCharsets.UTF_8))
                .doOnError(e -> {
                    throw new InvalidCertificateException(
                            "Failed to load certificate from %s".formatted(authenticationProviderProperties.url()));
                })
                .block();
    }
}
