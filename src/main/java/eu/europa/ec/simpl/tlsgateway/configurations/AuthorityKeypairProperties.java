package eu.europa.ec.simpl.tlsgateway.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "keypair")
public record AuthorityKeypairProperties(String algorithm) {}
