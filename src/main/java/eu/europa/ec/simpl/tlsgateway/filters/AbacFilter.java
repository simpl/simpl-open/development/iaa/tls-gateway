package eu.europa.ec.simpl.tlsgateway.filters;

import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.security.RouteConfig;
import eu.europa.ec.simpl.tlsgateway.configurations.security.Rule;
import java.util.function.Predicate;
import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Component
public class AbacFilter extends OrderedGlobalFilter {

    private final RouteConfig routeConfig;

    public AbacFilter(RouteConfig routeConfig, GlobalFilterProperties globalFilterProperties) {
        super(globalFilterProperties);
        this.routeConfig = routeConfig;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("Abac Filter...");

        return Flux.fromIterable(routeConfig.abac())
                .filterWhen(rule -> rule.isMatch(exchange))
                .filter(Rule::hasAttributes)
                .filter(Predicate.not(Rule::disableEphemeralProofCheck))
                .next()
                .switchIfEmpty(Mono.defer(() -> {
                    log.info(
                            "No ABAC privilege required for path {}",
                            exchange.getRequest().getPath());
                    return Mono.empty();
                }))
                .flatMap(rule -> rule.checkPrivilege(exchange))
                .then(chain.filter(exchange));
    }
}
