package eu.europa.ec.simpl.tlsgateway.configurations.publickey;

import eu.europa.ec.simpl.tlsgateway.configurations.ClientProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.microservices.UsersRolesProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("participant")
public class ParticipantPublicKeyProducer extends AbstractPublicKeyProducer {

    private final ClientProperties clientProperties;

    protected ParticipantPublicKeyProducer(
            UsersRolesProperties usersRolesProperties, ClientProperties clientProperties) {
        super(usersRolesProperties);
        this.clientProperties = clientProperties;
    }

    @Override
    protected String getAuthorityPublicKeyBaseUrl() {
        return "%s/user-api".formatted(clientProperties.authorityUrl());
    }
}
