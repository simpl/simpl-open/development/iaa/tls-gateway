package eu.europa.ec.simpl.tlsgateway.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class NoCertificateException extends StatusException {
    public NoCertificateException() {
        super(HttpStatus.FORBIDDEN, "No Certificate found in request");
    }
}
