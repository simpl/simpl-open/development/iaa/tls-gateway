package eu.europa.ec.simpl.tlsgateway.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;

@Profile("participant")
@ConfigurationProperties(prefix = "client")
public record ClientProperties(String authorityUrl) {}
