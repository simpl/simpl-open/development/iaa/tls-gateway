package eu.europa.ec.simpl.tlsgateway.utils;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.utils.Sha384Converter;
import eu.europa.ec.simpl.tlsgateway.exceptions.NoCertificateException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Optional;
import lombok.experimental.UtilityClass;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.SslInfo;
import org.springframework.web.server.ServerWebExchange;

@UtilityClass
public class ExchangeUtil {

    public static X509Certificate getCertificateFromRequest(ServerHttpRequest request) {
        return Optional.ofNullable(request)
                .map(ServerHttpRequest::getSslInfo)
                .map(SslInfo::getPeerCertificates)
                .filter(l -> l.length > 0)
                .map(i -> i[0])
                .orElseThrow(NoCertificateException::new);
    }

    public static X509Certificate getCertificateFromRequest(ServerWebExchange exchange) {
        return Optional.ofNullable(exchange)
                .map(ServerWebExchange::getRequest)
                .map(ExchangeUtil::getCertificateFromRequest)
                .orElseThrow();
    }

    public static String getCredentialIdFromExchange(ServerWebExchange exchange) {
        var cert = ExchangeUtil.getCertificateFromRequest(exchange);
        return Sha384Converter.toSha384(cert.getPublicKey());
    }

    public static String getCommonNameFromSubject(X509Certificate cert) {
        try {
            var x500name = new JcaX509CertificateHolder(cert).getSubject();
            var cn = x500name.getRDNs(BCStyle.CN)[0];
            return IETFUtils.valueToString(cn.getFirst().getValue());
        } catch (CertificateEncodingException e) {
            throw new RuntimeWrapperException(e);
        }
    }
}
