package eu.europa.ec.simpl.tlsgateway.configurations.ssl;

import eu.europa.ec.simpl.common.utils.CredentialUtil;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
import org.springframework.boot.web.server.Ssl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Configuration
public class SslConfig {

    private final SslProperties sslProperties;
    private final AuthenticationProviderClient authenticationProviderClient;

    public SslConfig(SslProperties sslProperties, AuthenticationProviderClient authenticationProviderClient) {
        this.sslProperties = sslProperties;
        this.authenticationProviderClient = authenticationProviderClient;
    }

    @Bean
    @SuppressWarnings("java:S2259")
    public Ssl ssl() throws IOException, CertificateException, KeyStoreException, NoSuchAlgorithmException {
        log.debug("Start building ssl context");
        var ssl = new Ssl();
        ssl.setEnabled(sslProperties.enabled());
        if (ssl.isEnabled()) {
            ssl.setClientAuth(sslProperties.clientAuth());

            var rawKeyStore = authenticationProviderClient.loadPemCertificates();

            var privateKey = authenticationProviderClient.loadPrivateKey();

            var keyStore = CredentialUtil.loadCredential(
                    new ByteArrayInputStream(rawKeyStore.getBytes(StandardCharsets.UTF_8)), privateKey);
            var trustStore = CredentialUtil.buildTrustStore(keyStore);

            writeKeyStore(keyStore);
            writeTrustStore(trustStore);

            setKeystoreProperties(ssl);
            setTrustStoreProperties(ssl);
        }
        log.debug("Finish building ssl context");
        return ssl;
    }

    @Bean
    public NettyReactiveWebServerFactory reactiveWebServerFactory(Ssl ssl) {
        NettyReactiveWebServerFactory factory = new NettyReactiveWebServerFactory();
        factory.setSsl(ssl);
        return factory;
    }

    private void writeKeyStore(KeyStore keyStore)
            throws IOException, CertificateException, KeyStoreException, NoSuchAlgorithmException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            keyStore.store(
                    baos,
                    Optional.ofNullable(sslProperties.keyStorePassword())
                            .map(String::toCharArray)
                            .orElse(null));
            writeFile(Path.of(sslProperties.keyStoreLocation()), baos.toByteArray());
        }
    }

    private void setKeystoreProperties(Ssl ssl) {
        ssl.setKeyStore(sslProperties.keyStoreLocation());
        ssl.setKeyStorePassword(sslProperties.keyStorePassword());
        ssl.setKeyStoreType(sslProperties.keyStoreType());
    }

    private void writeTrustStore(KeyStore truststore)
            throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

            truststore.store(
                    baos,
                    Optional.ofNullable(sslProperties.trustStorePassword())
                            .map(String::toCharArray)
                            .orElse(null));
            writeFile(Path.of(sslProperties.trustStoreLocation()), baos.toByteArray());
        }
    }

    private void setTrustStoreProperties(Ssl ssl) {
        ssl.setTrustStore(sslProperties.trustStoreLocation());
        ssl.setTrustStorePassword(sslProperties.trustStorePassword());
        ssl.setTrustStoreType(sslProperties.trustStoreType());
    }

    private void writeFile(Path path, byte[] content) throws IOException {
        var parentPath = path.getParent();
        if (parentPath != null && Files.notExists(parentPath)) {
            Files.createDirectories(parentPath);
        }
        Files.write(path, content, StandardOpenOption.CREATE);
    }
}
