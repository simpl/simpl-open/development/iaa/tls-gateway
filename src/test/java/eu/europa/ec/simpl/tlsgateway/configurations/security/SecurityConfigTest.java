package eu.europa.ec.simpl.tlsgateway.configurations.security;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class SecurityConfigTest {

    @MockitoBean(answers = Answers.RETURNS_DEEP_STUBS)
    private RouteConfig routeConfig;

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    void loadContest() {
        var rule = mock(Rule.class);
        when(rule.method()).thenReturn(HttpMethod.GET);
        when(rule.path()).thenReturn("/junitpath");
        var publicUrls = List.of(rule);
        when(routeConfig.publicUrls()).thenReturn(publicUrls);
        when(routeConfig.deniedUrls()).thenReturn(publicUrls);
        try (var ac = new AnnotationConfigApplicationContext()) {
            ac.setParent(applicationContext);
            ac.registerBean(SecurityConfig.class);
            ac.refresh();
        }
    }
}
