package eu.europa.ec.simpl.tlsgateway.configurations;

import static org.mockito.Mockito.mock;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.server.ServerWebExchange;

class LoggingRuleTest {

    @Test
    void matchesTest() {
        var rule = createDefaultLoggingRule();
        var exchange = mock(ServerWebExchange.class);
        rule.matches(exchange);
    }

    @Test
    void matchQueryTest() {
        var rule = createDefaultLoggingRule();
        var exchange = mock(ServerWebExchange.class, Answers.RETURNS_DEEP_STUBS);
        rule.matchQuery(exchange);
    }

    @Test
    void matchHeadersTest() {
        var rule = createDefaultLoggingRule();
        var exchange = mock(ServerWebExchange.class, Answers.RETURNS_DEEP_STUBS);
        rule.matchHeaders(exchange);
    }

    @Test
    void multimapRuleMatches() {
        var rule = createDefaultLoggingRule();
        var multimapRule = rule.header().getFirst();
        var queryParams = new HttpHeaders();
        queryParams.add("headerName", "headerValue");
        multimapRule.matches(queryParams);
    }

    public LoggingRule createDefaultLoggingRule() {
        var config = new LoggingRule.Config();
        config.setMessage("Message");
        config.setOperations(List.of());
        var multiMapRule = new LoggingRule.MultiMapRule("name", "value", List.of());
        var multiMapRuleHeader = new LoggingRule.MultiMapRule("nameHeader", "", List.of());
        return new LoggingRule(HttpMethod.GET, "/path", List.of(multiMapRule), List.of(multiMapRuleHeader), config);
    }
}
