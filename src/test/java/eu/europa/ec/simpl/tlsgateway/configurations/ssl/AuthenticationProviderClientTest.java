package eu.europa.ec.simpl.tlsgateway.configurations.ssl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.tlsgateway.configurations.AuthorityKeypairProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.microservices.AuthenticationProviderProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;

@ExtendWith(MockitoExtension.class)
class AuthenticationProviderClientTest {

    @Mock
    private AuthenticationProviderProperties authenticationProviderProperties;

    @Mock
    private AuthorityKeypairProperties authorityKeypairProperties;

    private AuthenticationProviderClient authenticationProviderClient;

    @BeforeEach
    public void init() {
        when(authenticationProviderProperties.url()).thenReturn("http://authenticationprovider.junit.local");
        authenticationProviderClient =
                new AuthenticationProviderClient(authenticationProviderProperties, authorityKeypairProperties);
    }

    @Test
    void loadPrivateKeyTest() {
        try (var webClient = mockStatic(WebClient.class, RETURNS_DEEP_STUBS)) {
            assertDoesNotThrow(() -> authenticationProviderClient.loadPrivateKey());
            verify(WebClient.builder()).baseUrl("http://authenticationprovider.junit.local/v1/keypairs");
        }
    }

    @Test
    void loadPemCertificatesTest() {
        try (var webClient = mockStatic(WebClient.class, RETURNS_DEEP_STUBS)) {
            assertDoesNotThrow(() -> authenticationProviderClient.loadPemCertificates());
            verify(WebClient.builder()).baseUrl("http://authenticationprovider.junit.local/v1/credentials/download");
        }
    }
}
