package eu.europa.ec.simpl.tlsgateway.configurations.ssl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.utils.CredentialUtil;
import eu.europa.ec.simpl.tlsgateway.configurations.microservices.AuthenticationProviderProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.microservices.UsersRolesProperties;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.server.Ssl;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
// @Import(SslConfig.class)
@EnableConfigurationProperties({
    SslProperties.class,
    UsersRolesProperties.class,
    AuthenticationProviderProperties.class,
})
@TestPropertySource(
        properties = {
            "keypair.algorithm=EC",
            "custom-ssl.enabled=true",
            // Ssl.ClientAuth clientAuth,
            "users-roles.url=http://usersroles.junit.local",
            "authentication-provider.url=authenticationprovider.junit.local",
        })
class SslConfigTest {

    @Autowired
    private SslProperties sslProperties;

    @MockitoBean
    private AuthenticationProviderClient authenticationProviderClient;

    @Autowired
    private UsersRolesProperties usersRolesProperties;

    @Autowired
    private AuthenticationProviderProperties authenticationProviderProperties;

    @DynamicPropertySource
    public static void dynamicProperties(DynamicPropertyRegistry dynamicPropertyRegistry) throws Exception {
        var tmpKeyStorePwd = "KEYSTORETMPPS";
        var tmpKeyStoreFile = Files.createTempFile("junit", "").toFile();
        var tmpKeyTrustStoreFile = Files.createTempFile("junit", "").toFile();
        dynamicPropertyRegistry.add("custom-ssl.keyStoreLocation", () -> tmpKeyStoreFile.getAbsolutePath());
        dynamicPropertyRegistry.add("custom-ssl.keyStorePassword", () -> tmpKeyStorePwd);
        dynamicPropertyRegistry.add("custom-ssl.trustStoreLocation", () -> tmpKeyTrustStoreFile.getAbsolutePath());
        dynamicPropertyRegistry.add("custom-ssl.trustStorePassword", () -> tmpKeyStorePwd);
        dynamicPropertyRegistry.add("custom-ssl.keyStoreType", () -> KeyStore.getDefaultType());
        dynamicPropertyRegistry.add("custom-ssl.trustStoreType", () -> KeyStore.getDefaultType());
    }

    @Test
    void ssl_whenSslIsEnabled_ThenDefineKeyStoreAndTrustStore()
            throws CertificateException, KeyStoreException, NoSuchAlgorithmException, IOException {
        var sslConfig = createDefaultSslConfig();
        var rawKeyStore = "junit pem certificate...";
        var privateKey = mock(PrivateKey.class);
        var keyStore = retrieveTestStore();
        var trustStore = retrieveTestStore();
        when(authenticationProviderClient.loadPemCertificates()).thenReturn(rawKeyStore);
        when(authenticationProviderClient.loadPrivateKey()).thenReturn(privateKey);

        try (var credentialUtil = mockStatic(CredentialUtil.class)) {
            when(CredentialUtil.loadCredential(any(), eq(privateKey))).thenReturn(keyStore);
            when(CredentialUtil.buildTrustStore(eq(keyStore))).thenReturn(trustStore);
            assertDoesNotThrow(() -> sslConfig.ssl());
        }
    }

    @Test
    void ssl_whenSslIsNotEnabled_ThenDoNotDefineKeyStoreAndTrustStore()
            throws CertificateException, KeyStoreException, NoSuchAlgorithmException, IOException {
        var sslConfig = new SslConfig(
                new SslProperties(
                        false,
                        sslProperties.keyStoreLocation(),
                        sslProperties.keyStorePassword(),
                        sslProperties.clientAuth(),
                        sslProperties.trustStoreLocation(),
                        sslProperties.trustStorePassword(),
                        sslProperties.keyStoreType(),
                        sslProperties.trustStoreType()),
                authenticationProviderClient);
        var rawKeyStore = "junit pem certificate...";
        var privateKey = mock(PrivateKey.class);
        var keyStore = retrieveTestStore();
        var trustStore = retrieveTestStore();
        when(authenticationProviderClient.loadPemCertificates()).thenReturn(rawKeyStore);
        when(authenticationProviderClient.loadPrivateKey()).thenReturn(privateKey);

        try (var credentialUtil = mockStatic(CredentialUtil.class)) {
            when(CredentialUtil.loadCredential(any(), eq(privateKey))).thenReturn(keyStore);
            when(CredentialUtil.buildTrustStore(eq(keyStore))).thenReturn(trustStore);
            assertDoesNotThrow(() -> sslConfig.ssl());
        }
    }

    @Test
    void reactiveWebServerFactoryTest() {
        var sslConfig = createDefaultSslConfig();
        var ssl = mock(Ssl.class);
        assertDoesNotThrow(() -> sslConfig.reactiveWebServerFactory(ssl));
    }

    private SslConfig createDefaultSslConfig() {
        return new SslConfig(sslProperties, authenticationProviderClient);
    }

    private KeyStore retrieveTestStore() {
        try {
            var ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, null);
            return ks;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
