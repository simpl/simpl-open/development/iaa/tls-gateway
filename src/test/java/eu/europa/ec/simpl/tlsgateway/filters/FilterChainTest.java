package eu.europa.ec.simpl.tlsgateway.filters;

import static eu.europa.ec.simpl.common.test.MockUtil.spyLambda;
import static eu.europa.ec.simpl.tlsgateway.filters.EphemeralProofFilter.MANAGE_EPHEMERAL_PROOF;
import static eu.europa.ec.simpl.tlsgateway.filters.EphemeralProofFilter.VALID_EPHEMERAL_PROOF;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.api.authenticationprovider.v1.model.CredentialDTO;
import eu.europa.ec.simpl.client.util.CertificateRevocation;
import eu.europa.ec.simpl.common.exceptions.EphemeralProofNotFoundException;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.redis.entity.EphemeralProof;
import eu.europa.ec.simpl.common.test.TestCertificateUtil;
import eu.europa.ec.simpl.common.utils.Sha384Converter;
import eu.europa.ec.simpl.tlsgateway.configurations.AuthorityKeypairProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.security.RouteConfig;
import eu.europa.ec.simpl.tlsgateway.exceptions.AbacException;
import eu.europa.ec.simpl.tlsgateway.exceptions.NoCertificateException;
import eu.europa.ec.simpl.tlsgateway.repositories.EphemeralProofRepository;
import eu.europa.ec.simpl.tlsgateway.services.TierOneSessionValidator;
import eu.europa.ec.simpl.tlsgateway.utils.JwtEphemeralProofVerifier;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.handler.FilteringWebHandler;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.server.ServerWebExchange;

@ExtendWith(MockitoExtension.class)
class FilterChainTest {

    static RouteConfig routeConfig;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    GlobalFilterProperties globalFilterProperties;

    @Mock
    AuthorityKeypairProperties authorityKeypairProperties;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    JwtEphemeralProofVerifier ephemeralProofVerifier;

    Function<String, JwtEphemeralProofVerifier> ephemeralProofVerifierFactory = spyLambda(s -> ephemeralProofVerifier);

    @Mock
    CertificateRevocation certificateRevocation;

    Supplier<CertificateRevocation> certificateRevocationFactory = spyLambda(() -> certificateRevocation);

    @Mock
    TierOneSessionValidator tierOneSessionValidator;

    @Mock
    EphemeralProofRepository ephemeralProofRepository;

    @BeforeAll
    static void addBouncyCastleProvider() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @BeforeAll
    static void loadRouteConfig() {
        YamlPropertiesFactoryBean yamlPropertiesFactoryBean = new YamlPropertiesFactoryBean();
        yamlPropertiesFactoryBean.setResources(new ClassPathResource("routes.yml"));
        ConfigurationPropertySource propertySource =
                new MapConfigurationPropertySource(yamlPropertiesFactoryBean.getObject());
        Binder binder = new Binder(propertySource);
        routeConfig = binder.bind("routes", RouteConfig.class).get();
    }

    @Test
    void withNoCertificate_whenEphemeralProofCheckIsDisabled_shouldThrowNoCertificateException() {
        // Given
        var exchange = new MockExchangeBuilder("/no-ephemeral-proof").build();
        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isInstanceOf(NoCertificateException.class);
    }

    @Test
    void withValidCertificate_withNoEphemeralProof_whenEphemeralProofCheckIsDisabled_shouldSucceed()
            throws CertificateException {
        // Given
        var exchange =
                new MockExchangeBuilder("/no-ephemeral-proof").withCertificate().build();
        willDoNothing().given(certificateRevocation).verify(any());
        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isNull();
        then(tierOneSessionValidator).should(never()).validate(any(SignedJWT.class), any());
    }

    @Test
    void
            withValidCertificate_withNoEphemeralProof_withTierOneToken_whenEphemeralProofCheckIsDisabled_shouldNotValidateTierOneSession()
                    throws CertificateException {
        // Given
        var exchange = new MockExchangeBuilder("/no-ephemeral-proof")
                .withCertificate()
                .withTierOneToken("ATTR")
                .build();
        willDoNothing().given(certificateRevocation).verify(any());
        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isNull();
        then(tierOneSessionValidator).should(never()).validate(any(SignedJWT.class), any());
    }

    @Test
    void
            withValidCertificate_withNoEphemeralProof_withTierOneTokenHavingNoAttributeList_whenEphemeralProofCheckIsDisabled_shouldNotThrowNullPointerException()
                    throws CertificateException {
        // Given
        var exchange = new MockExchangeBuilder("/no-ephemeral-proof")
                .withCertificate()
                .withTierOneToken((List<String>) null)
                .build();
        willDoNothing().given(certificateRevocation).verify(any());
        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isNull();
        then(tierOneSessionValidator).should(never()).validate(any(SignedJWT.class), any());
    }

    @Test
    void withValidCertificate_withNoEphemeralProof_whenNoAbacRule_shouldThrow() {
        // Given
        var exchange = new MockExchangeBuilder("/no-rule").withCertificate().build();
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.empty());

        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isInstanceOf(EphemeralProofNotFoundException.class);
    }

    @Test
    void
            withValidCertificate_withNoEphemeralProof_whenEphemeralProofCheckIsEnabled_shouldThrow_EphemeralProofNotFoundException() {
        // Given
        var exchange = new MockExchangeBuilder("/ephemeral-proof-required")
                .withCertificate()
                .build();
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.empty());

        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isInstanceOf(EphemeralProofNotFoundException.class);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
    }

    @Test
    void withValidCertificate_withValidEphemeralProof_withNoAttributes_whenNoAttributeIsRequired_shouldNotDoOCSPCheck()
            throws CertificateException, JOSEException {
        // Given
        var exchangeBuilder = new MockExchangeBuilder("/ephemeral-proof-required").withCertificate();
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.of(Instancio.create(EphemeralProof.class)));
        given(authorityKeypairProperties.algorithm()).willReturn("EC");

        var exchange = exchangeBuilder.build();
        given(ephemeralProofVerifier.verify(any())).willReturn(true);
        given(ephemeralProofVerifier.getPublicKeys().getFirst())
                .willReturn(Sha384Converter.toSha384(
                        exchangeBuilder.getSslInfo().getPeerCertificates()[0].getPublicKey()));
        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isNull();
        assertThat(exchange.getAttributes()).containsEntry(VALID_EPHEMERAL_PROOF, Boolean.TRUE);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
        then(certificateRevocation).should(never()).verify(any());
    }

    @Test
    void
            withValidCertificate_withValidEphemeralProof_withNoAttributes_whenAttributeIsRequired_shouldThrowAbacException()
                    throws CertificateException, JOSEException {
        // Given
        var exchangeBuilder = new MockExchangeBuilder("/abac").withCertificate();
        var exchange = exchangeBuilder.build();
        given(authorityKeypairProperties.algorithm()).willReturn("EC");
        given(ephemeralProofVerifier.verify(any())).willReturn(true);
        given(ephemeralProofVerifier.getPublicKeys().getFirst())
                .willReturn(Sha384Converter.toSha384(
                        exchangeBuilder.getSslInfo().getPeerCertificates()[0].getPublicKey()));
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.of(Instancio.create(EphemeralProof.class)));

        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isInstanceOf(AbacException.class);
        assertThat(exchange.getAttributes()).containsEntry(VALID_EPHEMERAL_PROOF, Boolean.TRUE);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
        then(certificateRevocation).should(never()).verify(any());
    }

    @Test
    void
            withValidCertificate_withValidEphemeralProof_withDifferentAttribute_whenAttributeIsRequired_shouldThrowAbacException()
                    throws CertificateException, JOSEException {
        // Given
        var exchangeBuilder = new MockExchangeBuilder("/abac").withCertificate();
        var exchange = exchangeBuilder.build();
        given(authorityKeypairProperties.algorithm()).willReturn("EC");
        given(ephemeralProofVerifier.verify(any())).willReturn(true);
        given(ephemeralProofVerifier.getPublicKeys().getFirst())
                .willReturn(Sha384Converter.toSha384(
                        exchangeBuilder.getSslInfo().getPeerCertificates()[0].getPublicKey()));
        given(ephemeralProofVerifier.getIdentityAttributes()).willReturn(getParticipantIdentityAttributes("IATTR_2"));
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.of(Instancio.create(EphemeralProof.class)));

        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isInstanceOf(AbacException.class);
        assertThat(exchange.getAttributes()).containsEntry(VALID_EPHEMERAL_PROOF, Boolean.TRUE);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
        then(certificateRevocation).should(never()).verify(any());
    }

    @Test
    void withValidCertificate_withValidEphemeralProof_withRequiredAttribute_shouldSucceed()
            throws CertificateException, JOSEException {
        // Given
        var exchangeBuilder = new MockExchangeBuilder("/abac").withCertificate();
        var exchange = exchangeBuilder.build();
        given(authorityKeypairProperties.algorithm()).willReturn("EC");
        given(ephemeralProofVerifier.verify(any())).willReturn(true);
        given(ephemeralProofVerifier.getPublicKeys().getFirst())
                .willReturn(Sha384Converter.toSha384(
                        exchangeBuilder.getSslInfo().getPeerCertificates()[0].getPublicKey()));
        given(ephemeralProofVerifier.getIdentityAttributes()).willReturn(getParticipantIdentityAttributes("ATTR_1"));
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.of(Instancio.create(EphemeralProof.class)));

        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isNull();
        assertThat(exchange.getAttributes()).containsEntry(VALID_EPHEMERAL_PROOF, Boolean.TRUE);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
        then(certificateRevocation).should(never()).verify(any());
    }

    @Test
    void
            withValidCertificate_withValidEphemeralProof_withRequiredAttributeInEphemeralProofButNotInTier1Token_shouldThrowAbacException()
                    throws CertificateException, JOSEException {
        // Given
        var exchangeBuilder = new MockExchangeBuilder("/abac").withCertificate().withTierOneToken();

        var exchange = exchangeBuilder.build();
        given(authorityKeypairProperties.algorithm()).willReturn("EC");
        given(ephemeralProofVerifier.verify(any())).willReturn(true);
        given(ephemeralProofVerifier.getPublicKeys().getFirst())
                .willReturn(Sha384Converter.toSha384(
                        exchangeBuilder.getSslInfo().getPeerCertificates()[0].getPublicKey()));
        given(ephemeralProofVerifier.getIdentityAttributes()).willReturn(getUserIdentityAttributes("ATTR_1"));
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.of(Instancio.create(EphemeralProof.class)));
        willDoNothing().given(tierOneSessionValidator).validate(any(SignedJWT.class), any());
        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isInstanceOf(AbacException.class);
        assertThat(exchange.getAttributes()).containsEntry(VALID_EPHEMERAL_PROOF, Boolean.TRUE);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
        then(certificateRevocation).should(never()).verify(any());
    }

    @Test
    void
            withValidCertificate_withValidEphemeralProof_withRequiredAttributeNotAssignableToRolesInEphemeralProofButNotInTier1Token_shouldSucceed()
                    throws JOSEException, CertificateException {
        var exchangeBuilder =
                new MockExchangeBuilder("/abac-not-tier-one").withCertificate().withTierOneToken();
        var exchange = exchangeBuilder.build();
        given(authorityKeypairProperties.algorithm()).willReturn("EC");
        given(ephemeralProofVerifier.verify(any())).willReturn(true);
        given(ephemeralProofVerifier.getPublicKeys().getFirst())
                .willReturn(Sha384Converter.toSha384(
                        exchangeBuilder.getSslInfo().getPeerCertificates()[0].getPublicKey()));
        given(ephemeralProofVerifier.getIdentityAttributes())
                .willReturn(getParticipantIdentityAttributes("NOT_ASSIGNABLE_TO_ROLES"));
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.of(Instancio.create(EphemeralProof.class)));
        willDoNothing().given(tierOneSessionValidator).validate(any(SignedJWT.class), any());
        // When
        var exception = catchException(() -> runFilterChain(exchange));

        assertThat(exception).isNull();
        assertThat(exchange.getAttributes()).containsEntry(VALID_EPHEMERAL_PROOF, Boolean.TRUE);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
        then(certificateRevocation).should(never()).verify(any());
    }

    @Test
    void
            withValidCertificate_withValidEphemeralProof_withRequiredAttributeInEphemeralProofAndInTier1Token_shouldSucceed()
                    throws CertificateException, JOSEException {
        // Given
        var exchangeBuilder = new MockExchangeBuilder("/abac").withCertificate().withTierOneToken("ATTR_1");

        var exchange = exchangeBuilder.build();
        given(authorityKeypairProperties.algorithm()).willReturn("EC");
        given(ephemeralProofVerifier.verify(any())).willReturn(true);
        given(ephemeralProofVerifier.getPublicKeys().getFirst())
                .willReturn(Sha384Converter.toSha384(
                        exchangeBuilder.getSslInfo().getPeerCertificates()[0].getPublicKey()));
        given(ephemeralProofVerifier.getIdentityAttributes()).willReturn(getUserIdentityAttributes("ATTR_1"));
        given(ephemeralProofRepository.findById(any())).willReturn(Optional.of(Instancio.create(EphemeralProof.class)));

        willDoNothing().given(tierOneSessionValidator).validate(any(SignedJWT.class), any());
        // When
        var exception = catchException(() -> runFilterChain(exchange));
        // Then
        assertThat(exception).isNull();
        assertThat(exchange.getAttributes()).containsEntry(VALID_EPHEMERAL_PROOF, Boolean.TRUE);
        assertThat(exchange.getAttributes()).containsEntry(MANAGE_EPHEMERAL_PROOF, Boolean.TRUE);
        then(certificateRevocation).should(never()).verify(any());
    }

    private void runFilterChain(ServerWebExchange exchange) {
        var webHandler = new FilteringWebHandler(getFilters(), false);
        webHandler.handle(exchange).block();
    }

    private List<IdentityAttributeDTO> getUserIdentityAttributes(String... codes) {
        return buildIdentityAttributes(true, codes);
    }

    private List<IdentityAttributeDTO> getParticipantIdentityAttributes(String... codes) {
        return buildIdentityAttributes(false, codes);
    }

    private List<IdentityAttributeDTO> buildIdentityAttributes(boolean assignable, String... codes) {
        return Stream.of(codes)
                .map(code -> new IdentityAttributeDTO().setCode(code).setAssignableToRoles(assignable))
                .toList();
    }

    @SneakyThrows
    private List<GlobalFilter> getFilters() {

        return List.of(
                new EphemeralProofFilter(
                        routeConfig,
                        ephemeralProofRepository,
                        ephemeralProofVerifierFactory,
                        Instancio.create(CredentialDTO.class)
                                .setPublicKey(Base64.getEncoder()
                                        .encodeToString(TestCertificateUtil.generateKeyPair()
                                                .getPublic()
                                                .getEncoded())),
                        globalFilterProperties,
                        authorityKeypairProperties),
                new OcspFilter(certificateRevocationFactory, globalFilterProperties),
                new HeadersFilter(tierOneSessionValidator, globalFilterProperties),
                new AbacFilter(routeConfig, globalFilterProperties));
    }
}
