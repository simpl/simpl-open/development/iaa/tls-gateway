package eu.europa.ec.simpl.tlsgateway.utils;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import eu.europa.ec.simpl.common.ephemeralproof.JwtEphemeralProofParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JwtEphemeralProofVerifierTest {
    @Mock
    private JwtEphemeralProofParser jwtEphemeralProofParser;

    private JwtEphemeralProofVerifier jwtEphemeralProofVerifier;

    @BeforeEach
    public void init() {
        jwtEphemeralProofVerifier = new JwtEphemeralProofVerifier(jwtEphemeralProofParser);
    }

    @Test
    void getRawTest() {
        assertDoesNotThrow(() -> jwtEphemeralProofVerifier.getRaw());
    }

    @Test
    void getSubjectTest() {
        assertDoesNotThrow(() -> jwtEphemeralProofVerifier.getSubject());
    }

    @Test
    void getIdentityAttributesTest() {
        assertDoesNotThrow(() -> jwtEphemeralProofVerifier.getIdentityAttributes());
    }

    @Test
    void getPublicKeysTest() {
        assertDoesNotThrow(() -> jwtEphemeralProofVerifier.getPublicKeys());
    }

    @Test
    void getExpirationTest() {
        assertDoesNotThrow(() -> jwtEphemeralProofVerifier.getExpiration());
    }

    @Test
    void getClaimsSetTest() {
        assertDoesNotThrow(() -> jwtEphemeralProofVerifier.getClaimsSet());
    }
}
