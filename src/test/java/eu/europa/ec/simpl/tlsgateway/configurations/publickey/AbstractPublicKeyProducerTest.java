package eu.europa.ec.simpl.tlsgateway.configurations.publickey;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.withSettings;

import eu.europa.ec.simpl.tlsgateway.configurations.microservices.UsersRolesProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;

@ExtendWith(MockitoExtension.class)
class AbstractPublicKeyProducerTest {
    @Mock
    private UsersRolesProperties usersRolesProperties;

    private AbstractPublicKeyProducer abstractPublicKeyProducer;

    @BeforeEach
    public void init() {
        abstractPublicKeyProducer = mock(
                AbstractPublicKeyProducer.class,
                withSettings().useConstructor(usersRolesProperties).defaultAnswer(Answers.CALLS_REAL_METHODS));
    }

    @Test
    void authorityPublicKeyTest() {
        try (var webClient = mockStatic(WebClient.class, Answers.RETURNS_DEEP_STUBS)) {
            abstractPublicKeyProducer.authorityPublicKey();
        }
    }

    @Test
    void myPublicKeyTest() {
        try (var webClient = mockStatic(WebClient.class, Answers.RETURNS_DEEP_STUBS)) {
            abstractPublicKeyProducer.myPublicKey();
        }
    }
}
