package eu.europa.ec.simpl.tlsgateway.filters;

import static eu.europa.ec.simpl.common.test.TestCertificateUtil.anX509Certificate;
import static org.mockito.Mockito.when;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.SignedJWT;
import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.utils.JwtUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;
import lombok.Getter;
import org.bouncycastle.operator.OperatorCreationException;
import org.mockito.Mockito;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.http.server.reactive.SslInfo;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.web.server.ServerWebExchange;

public class MockExchangeBuilder {

    private final MockServerHttpRequest.BaseBuilder<?> request;

    @Getter
    private SslInfo sslInfo;

    public MockExchangeBuilder(String path) {
        request = MockServerHttpRequest.get(path);
    }

    public MockExchangeBuilder withCertificate() {
        sslInfo = new MockSslInfo();
        request.sslInfo(sslInfo);
        return this;
    }

    public MockExchangeBuilder withTierOneToken(List<String> identityAttributes) {
        SignedJWT jwt = null;
        try {
            var claims = new HashMap<String, Object>();
            claims.put("identity_attributes", identityAttributes);
            jwt = JwtUtil.createSignedJWT(claims);
        } catch (NoSuchAlgorithmException | JOSEException e) {
            throw new RuntimeWrapperException(e);
        }
        request.header("Authorization", "Bearer " + jwt.serialize());
        return this;
    }

    public MockExchangeBuilder withTierOneToken(String... identityAttributes) {
        return withTierOneToken(Arrays.stream(identityAttributes).toList());
    }

    public ServerWebExchange build() {
        var exchange = MockServerWebExchange.from(request);
        var route = Mockito.mock(Route.class);
        when(route.getFilters()).thenReturn(List.of());
        exchange.getAttributes().put(GATEWAY_ROUTE_ATTR, route);
        return exchange;
    }

    private static class MockSslInfo implements SslInfo {

        private final X509Certificate[] peerCertificates;

        private MockSslInfo() {
            try {
                var certFactory = CertificateFactory.getInstance("X.509");
                var certHolder =
                        anX509Certificate("CN=OnBoardingCA", "CN=%s, O=Applicant Entity".formatted(UUID.randomUUID()));
                var cert = (X509Certificate)
                        certFactory.generateCertificate(new ByteArrayInputStream(certHolder.getEncoded()));
                this.peerCertificates = new X509Certificate[] {cert};
            } catch (NoSuchAlgorithmException | OperatorCreationException | CertificateException | IOException e) {
                throw new RuntimeWrapperException(e);
            }
        }

        @Override
        public String getSessionId() {
            throw new IllegalStateException("Should never be called");
        }

        @Override
        public X509Certificate[] getPeerCertificates() {
            return peerCertificates;
        }
    }
}
