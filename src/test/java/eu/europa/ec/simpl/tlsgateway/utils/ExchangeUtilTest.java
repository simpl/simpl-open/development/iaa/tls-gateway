package eu.europa.ec.simpl.tlsgateway.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.Date;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;

class ExchangeUtilTest {

    @Test
    void getCertificateFromRequestServerHttpRequestTest() {
        var certificate = mock(X509Certificate.class);
        X509Certificate[] certificates = {certificate};
        var request = mock(ServerHttpRequest.class, Answers.RETURNS_DEEP_STUBS);
        when(request.getSslInfo().getPeerCertificates()).thenReturn(certificates);
        var result = ExchangeUtil.getCertificateFromRequest(request);
        assertThat(result).isEqualTo(certificate);
    }

    @Test
    void getCertificateFromRequestServerWebExchangeTest() {
        var certificate = mock(X509Certificate.class);
        X509Certificate[] certificates = {certificate};
        var exchange = mock(ServerWebExchange.class, Answers.RETURNS_DEEP_STUBS);
        when(exchange.getRequest().getSslInfo().getPeerCertificates()).thenReturn(certificates);
        var result = ExchangeUtil.getCertificateFromRequest(exchange);
        assertThat(result).isEqualTo(certificate);
    }

    @Test
    void getCredentialIdFromExchangeTest() {
        var certificate = mock(X509Certificate.class, Answers.RETURNS_DEEP_STUBS);
        when(certificate.getPublicKey().getEncoded()).thenReturn("junitshacertificate".getBytes());
        X509Certificate[] certificates = {certificate};
        var exchange = mock(ServerWebExchange.class, Answers.RETURNS_DEEP_STUBS);
        when(exchange.getRequest().getSslInfo().getPeerCertificates()).thenReturn(certificates);
        ExchangeUtil.getCredentialIdFromExchange(exchange);
        var result = ExchangeUtil.getCredentialIdFromExchange(exchange);
        assertThat(result).isEqualTo("TtYvf4+9zDYzGDwJlWl9/HLA6ho4/lP89vwtusFjz+aMPmsNRO/KDZN+tIrBqZlX");
    }

    @Test
    void getCommonNameFromSubjectTest() {
        var cert = generateCertificate();
        ExchangeUtil.getCommonNameFromSubject(cert);
    }

    private X509Certificate generateCertificate() {
        try {
            Security.addProvider(new BouncyCastleProvider());
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            X500Name issuer = new X500Name("CN=Test CA, O=My Organization, L=My City, C=IT");
            X500Name subject = new X500Name("CN=Test Subject, O=My Organization, L=My City, C=IT");
            BigInteger serial = BigInteger.valueOf(System.currentTimeMillis());
            Date notBefore = new Date();
            Date notAfter = new Date(System.currentTimeMillis() + (365 * 24 * 60 * 60 * 1000L));
            X509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(
                    issuer, // Emittente
                    serial, // Numero seriale
                    notBefore, // Valido da
                    notAfter, // Valido fino a
                    subject, // Soggetto
                    keyPair.getPublic() // Chiave pubblica
                    );
            ContentSigner signer = new JcaContentSignerBuilder("SHA256WithRSAEncryption").build(keyPair.getPrivate());
            return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certBuilder.build(signer));

        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
