package eu.europa.ec.simpl.tlsgateway.configurations;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties({
    AuthorityKeypairProperties.class,
})
@TestPropertySource(properties = {"keypair.algorithm=EC"})
class AuthorityKeypairPropertiesTest {

    @Autowired
    AuthorityKeypairProperties properties;

    @Test
    void validateAuthorityKeypairProperties() {
        Assertions.assertThat(properties).hasNoNullFieldsOrProperties();
    }
}
