package eu.europa.ec.simpl.tlsgateway.exceptions;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

class AbacExceptionTest {

    @Test
    void contructorTest() {
        var rolePrivileges = List.of("p1", "p2", "p3");
        var expection = new AbacException(rolePrivileges);
        assertThat(expection.getMessage().contains("p1")).isTrue();
        assertThat(expection.getMessage().contains("p2")).isTrue();
        assertThat(expection.getMessage().contains("p3")).isTrue();
    }
}
