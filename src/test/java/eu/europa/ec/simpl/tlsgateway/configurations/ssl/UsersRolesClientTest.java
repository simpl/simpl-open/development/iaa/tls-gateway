package eu.europa.ec.simpl.tlsgateway.configurations.ssl;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.tlsgateway.configurations.microservices.UsersRolesProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;

@ExtendWith(MockitoExtension.class)
class UsersRolesClientTest {

    @Mock
    private UsersRolesProperties usersRolesProperties;

    private UsersRolesClient usersRolesClient;

    @BeforeEach
    public void init() {
        when(usersRolesProperties.url()).thenReturn("http://usersroles.junit.local");
        usersRolesClient = new UsersRolesClient(usersRolesProperties);
    }

    @Test
    void loadPemCertificatesTest() {
        try (var webClient = mockStatic(WebClient.class, RETURNS_DEEP_STUBS)) {
            assertDoesNotThrow(() -> usersRolesClient.loadPemCertificates());
            verify(WebClient.builder()).baseUrl("http://usersroles.junit.local/credential/download");
        }
    }
}
