package eu.europa.ec.simpl.tlsgateway.filters;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.api.authenticationprovider.v1.model.CredentialDTO;
import eu.europa.ec.simpl.tlsgateway.configurations.GlobalFilterProperties;
import eu.europa.ec.simpl.tlsgateway.configurations.LoggingRule;
import eu.europa.ec.simpl.tlsgateway.configurations.security.RouteConfig;
import eu.europa.ec.simpl.tlsgateway.utils.ExchangeUtil;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class LoggingFilterTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private CredentialDTO myPublicKey;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private RouteConfig routeConfig;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private LoggingRule loggingRule;

    private LoggingFilter loggingFilter;

    @BeforeEach
    public void init() {
        var loggingsRules = List.of(loggingRule);
        when(routeConfig.logging().business()).thenReturn(loggingsRules);
        var globalFilterProperties = mock(GlobalFilterProperties.class);
        loggingFilter = new LoggingFilter(myPublicKey, routeConfig, globalFilterProperties);
    }

    @Test
    void filter_whenRuleDoesNotMatch_thenDoesNotThrow() {
        var exchange = mock(ServerWebExchange.class);
        var chain = mock(GatewayFilterChain.class);
        when(loggingRule.matches(exchange)).thenReturn(Mono.just(true));
        when(chain.filter(exchange)).thenReturn(Mono.empty());
        assertDoesNotThrow(() -> loggingFilter.filter(exchange, chain));
    }

    @Test
    void businessLoggerLogTest() throws NoSuchAlgorithmException {
        LoggingRule.Config config = mock(LoggingRule.Config.class);
        ServerWebExchange exchange = mock(ServerWebExchange.class, Answers.RETURNS_DEEP_STUBS);
        var request = mock(ServerHttpRequest.class, Answers.RETURNS_DEEP_STUBS);
        when(exchange.getRequest()).thenReturn(request);
        when(request.getMethod()).thenReturn(HttpMethod.GET);
        when(request.getPath().value()).thenReturn("http://junit/path");
        GatewayFilterChain chain = mock(GatewayFilterChain.class);
        var headers = new HttpHeaders();
        headers.set("authentication", "Bearer token");
        when(exchange.getRequest().getHeaders()).thenReturn(headers);

        var keyPair = KeyPairGenerator.getInstance("RSA").genKeyPair();
        var pubBase64 = Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
        when(myPublicKey.getPublicKey()).thenReturn(pubBase64);
        var businessLogger = loggingFilter.newBusinessLogger(config, exchange, chain);
        when(chain.filter(exchange)).thenReturn(Mono.empty());
        try (var exchangeUtil = mockStatic(ExchangeUtil.class)) {
            when(ExchangeUtil.getCredentialIdFromExchange(exchange)).thenReturn("junit-destination");
            businessLogger.log();
        }
    }
}
