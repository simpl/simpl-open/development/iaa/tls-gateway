package eu.europa.ec.simpl.tlsgateway.configurations;

import eu.europa.ec.simpl.tlsgateway.filters.EphemeralProofFilter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer;
import org.springframework.core.Ordered;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(GlobalFilterProperties.class)
@ContextConfiguration(initializers = ConfigDataApplicationContextInitializer.class)
class MicroservicePropertiesTest {

    @Autowired
    GlobalFilterProperties properties;

    @Test
    void validateMicroserviceProperties() {
        Assertions.assertThat(properties).hasNoNullFieldsOrProperties();
    }

    @Test
    void ephemeralProofFilter_shouldHaveHighestPrecedence() {
        Assertions.assertThat(properties.getOrder(EphemeralProofFilter.class)).isEqualTo(Ordered.HIGHEST_PRECEDENCE);
    }
}
